package cn.com.sina.rd.dsp.model;

import cn.com.sina.rd.dsp.tools.MoveOnce;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-10
 * Time: 下午3:36
 */
public class Cluster implements Weight{
    public static final String CLUSTER_NAME="cluster_name";
    public static final String HIGH_GATE ="high_gate";
    public static final String LOW_GATE ="low_gate";
    public static final String SERVERS="servers";

    private String name;

    private long highgate;
    private long lowgate;

    private final boolean acend;

    private TreeMap<Server,Long> maps= Maps.newTreeMap();

    public Cluster(){
        acend=true;
    }

    public Cluster(boolean acend){
        this.acend=acend;
    }

    public TreeMap<Server, Long> getMaps() {
        return maps;
    }

    public String getName() {
        return name;
    }

    public boolean isAcend() {
        return acend;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getHighgate() {
        return highgate;
    }


    public long getLowgate() {
        return highgate/10;
    }

    public void setLowgate(long lowgate) {
        this.lowgate = lowgate;
    }

    public void setHighgate(long highgate) {
        this.highgate = highgate;
        setLowgate(highgate/10);
    }

    public void add2Maps(Server ship) {
        maps.put(ship,ship.getWeight());
    }

    public static Cluster createByJSON(JSONObject json,boolean acend){
        Cluster cluster=new Cluster(acend);
        cluster.setName(json.getString(CLUSTER_NAME));
        cluster.setHighgate(json.getLong(HIGH_GATE));
        JSONArray array=json.getJSONArray(SERVERS);
        for(int i=0;i<array.size();i++){
            Server ship= Server.createByJson(array.getJSONObject(i), cluster.acend);
            cluster.add2Maps(ship);
        }
        return cluster;
    }

    public static JSONObject switch2JSON(Cluster cluster){
        JSONObject josn=new JSONObject();
        josn.put(CLUSTER_NAME,cluster.getName());
        josn.put(HIGH_GATE,cluster.getHighgate());
        JSONArray servers=new JSONArray();
        josn.put(SERVERS,servers);
        //many server
        for(Server server:cluster.getMaps().keySet()){
            JSONObject server_arr=new JSONObject();
            JSONArray region_arr=new JSONArray();
            //many load
            List<Load> loadList= Lists.newArrayList(server.getLoadMap().keySet());
            for(Load load:loadList){
                JSONObject single_load=new JSONObject();
                single_load.put(Load.LOAD_NANE, load.getName());
                single_load.put(Load.WEIGHT, load.getWeight());
                region_arr.add(single_load);
            }
            server_arr.put(server.getServerName(), region_arr);
            servers.add(server_arr);
        }
        return josn;
    }

    @Override
    public Long getWeight() {
        long total=0l;
        for(Long value:maps.values()){
            total+=value;
        }
        return total;
    }

    public static void main(String ...args){

    }

    public void move(MoveOnce moveOnce) {
        Server from=moveOnce.getFrom();
        Load load=moveOnce.getName();
        Server to=moveOnce.getTo();
        for(Server server:maps.keySet()){
            if(server.equals(from)){
                server.getLoadMap().remove(load);
            }else if(server.equals(to)){
                server.addLoadMap(load);
            }
        }
    }
}
