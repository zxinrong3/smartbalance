package cn.com.sina.rd.dsp.tools;

import cn.com.sina.rd.dsp.balancer.ShipLoad;
import cn.com.sina.rd.dsp.model.Cluster;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-11
 * Time: 上午10:50
 * To change this template use File | Settings | File Templates.
 */
public class ShowMove implements Iterator<JSONObject>{
    List<MoveOnce> list= Lists.newArrayList();
    Cluster showcluster=null;
    Iterator<MoveOnce> iterator=null;
    JSONObject currentJson=null;
    JSONObject result=new JSONObject();

    public ShowMove(JSONObject cluster_load){
        ShipLoad ship= new ShipLoad(Cluster.createByJSON(cluster_load,true));
        result=ship.makedecision();
        list=ship.getMoves();
        iterator=list.iterator();
        showcluster=Cluster.createByJSON(cluster_load,true);
    }


    public int getSize(){
        return list.size();
    }

    public JSONObject getResult() {
        return result;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public JSONObject next() {
        MoveOnce moveOnce=iterator.next();
        showcluster.move(moveOnce);
        currentJson=Cluster.switch2JSON(showcluster);
        return currentJson;
    }

    @Override
    public void remove() {
        iterator.remove();
    }
}
