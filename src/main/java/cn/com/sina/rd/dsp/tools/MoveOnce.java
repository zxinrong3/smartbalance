package cn.com.sina.rd.dsp.tools;

import cn.com.sina.rd.dsp.model.Load;
import cn.com.sina.rd.dsp.model.Server;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-10
 * Time: 下午9:51
 * To change this template use File | Settings | File Templates.
 */
public class MoveOnce {
    private Load name;
    private Server from;
    private Server to;

    public MoveOnce(Load name, Server from, Server to) {
        this.name = name;
        this.from = from;
        this.to = to;
//        System.out.println("move action:"+this);
    }

    public Load getName() {
        return name;
    }

    public Server getFrom() {
        return from;
    }

    public Server getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MoveOnce aMove = (MoveOnce) o;

        if (from != null ? !from.equals(aMove.from) : aMove.from != null) return false;
        if (name != null ? !name.equals(aMove.name) : aMove.name != null) return false;
        if (to != null ? !to.equals(aMove.to) : aMove.to != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (from != null ? from.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }

    @Override
    public String toString(){
        return this.getName()+":"+this.getFrom()+"->"+this.getTo();
    }
}
