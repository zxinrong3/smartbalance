package cn.com.sina.rd.dsp.balancer;

import cn.com.sina.rd.dsp.model.Cluster;
import cn.com.sina.rd.dsp.tools.MoveOnce;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-10
 * Time: 下午4:32
 */
public class doBalance {
    public static final String ACTION="ACTION";
    public static final String AVERAGE="AVERAGE_LOAD";
    public static final String BALANCE="BALANCE";

    public enum Action {
        Expand("Expand"),
        Shrink("Shrink"),
        Balance("Balance"),
        Split("Split");

        private String name;
        Action(String name){
            this.name=name;
        }

        @Override
        public String toString(){
            return this.name;
        }
    }

    public static JSONObject doBalance(JSONObject claster_load){
        JSONObject result=new JSONObject();
        Cluster cluster=Cluster.createByJSON(claster_load,true);
        ShipLoad shipLoad=new ShipLoad(cluster);
        result= shipLoad.makedecision();

        result.put(cluster.getName(),cluster.getMaps());
        return result;
    }
    public static List<MoveOnce> getMoveSteps(JSONObject claster_load){
        Cluster cluster=Cluster.createByJSON(claster_load,true);
        ShipLoad shipLoad=new ShipLoad(cluster);
        return shipLoad.getMoves();
    }
}
