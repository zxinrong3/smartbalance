package cn.com.sina.rd.dsp.model;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-10
 * Time: 上午11:57
 */
public class Server implements Comparable,Weight{
    private final boolean acend;

    private String serverName;
    private TreeMap<Load,Long> loadMap= Maps.newTreeMap();

    public Server(){
        acend=true;
    }

    public Server(boolean acend){
        this.acend=acend;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public Load getFirstLoad() {
        return loadMap.firstKey();
    }

    public Load getLastLoad() {
        if(loadMap.isEmpty()){
            return null;
        }
        return loadMap.lastKey();
    }

    public TreeMap<Load, Long> getLoadMap() {
        return loadMap;
    }

    public void addLoadMap( Load load) {
        this.loadMap.put(load,new Long(load.getWeight()));
    }

    public Long getWeight(){
        long value=0l;
        for(Load load:loadMap.keySet()){
            value+=load.getWeight();
        }
        return value;
    }

    @Override
    public int compareTo(Object o) {
        if (o != null && o instanceof Server){
            if(acend){
                int value=this.getWeight().compareTo(((Server) o).getWeight());
                if(value==0){
                    value=this.hashCode()-o.hashCode();
                }
                return value ;
            }else{
                int value= ((Server) o).getWeight().compareTo(this.getWeight());
                if(value==0){
                    value=this.hashCode()-o.hashCode();
                }
                return value;
            }
        }else {
            if(o==null){
                throw new NullPointerException(this+"compared to null ");
            }
            throw new RuntimeException("class type not match");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Server ship = (Server) o;

//        if (loadMap != null ? !loadMap.equals(ship.loadMap) : ship.loadMap != null) return false;
        if (serverName != null ? !serverName.equals(ship.serverName) : ship.serverName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = serverName != null ? serverName.hashCode() : 0;
//        result = 31 * result + (loadMap != null ? loadMap.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
//        return "Server{" +
//                "serverName='" + serverName + '\'' +
//                ", load=" + getWeight() +
//                '}';
        return "Server{" +
                "serverName='" + serverName + '\'' +
                ", load=" + getWeight() +
                ", loadMap=" + loadMap +
                '}';
    }

    @Override
    public Server clone(){
        Server server=new Server(this.acend);
        server.setServerName(this.serverName);
        server.loadMap=Maps.newTreeMap(this.loadMap);
        return server;
    }

    public static Server createByJson(JSONObject server,boolean acend) {
        Iterator<Map.Entry<String,Object>> it=server.entrySet().iterator();
        if(it.hasNext()){
            Map.Entry<String,Object> e=it.next();
            Server ship=new Server();  //server 永远降序
            ship.setServerName(e.getKey());
            JSONArray ja=null;
            if(e.getValue() instanceof  JSONArray){
                ja= (JSONArray) e.getValue();
            }else{
                ja= JSON.parseArray(e.getValue().toString());
            }
            for(int i=0;i< ja.size();i++){
                Load load=Load.createByJSON(ja.getJSONObject(i),acend);
                ship.addLoadMap(load);
            }

            return ship;
        }

        return null;
    }


    public static void main(String ...args){
    }
}
