package cn.com.sina.rd.dsp.model;

import com.alibaba.fastjson.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: xinrong3
 * Date: 14-1-10
 * Time: 上午11:58
 */
public class Load implements Comparable,Weight{
    public static final String LOAD_NANE="load_name";
    public static final String WEIGHT="weight";

    private String name;
    private Long weight;
    private final boolean acend;


    public Load(){
        acend=true;
    }


    public Load(boolean acend){
        this.acend=acend;
    }

    public static String getLoadNane() {
        return LOAD_NANE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Integer weight) {
        this.weight = weight.longValue();
    }

    public Long getWeight(){
        return weight;
    }

    @Override
    public int compareTo(Object o) {
        if(o!=null && o instanceof Load){
            if(acend){
                int value=this.weight.compareTo(((Load) o).getWeight());
                if(value==0){
                    value=this.hashCode()-o.hashCode();
                }
                return value;
            }else {
                int value=((Load) o).getWeight().compareTo(this.weight);
                if(value==0){
                    value=this.hashCode()-o.hashCode();
                }
                return value;
            }
        }else {
            if(o==null){
                throw new NullPointerException(this+"compared to null ");
            }
            throw new RuntimeException("wrong class ");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Load load = (Load) o;

        if (name != null ? !name.equals(load.name) : load.name != null) return false;
        if (weight != null ? !weight.equals(load.weight) : load.weight != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString(){
        return this.getName()+":"+this.getWeight();
    }
    public static Load createByJSON(JSONObject loadJson,boolean acend) {
        Load load=new Load(acend);
        load.setName(loadJson.getString(LOAD_NANE));
        load.setWeight(loadJson.getIntValue(WEIGHT));
        return load;
    }
}
